import Vue from 'vue'
import Router from 'vue-router'
import main from '@/components/main'
import errorPage from '@/components/pages/error'
import recipes from '@/components/pages/recipes'
import recipesDetails from '@/components/pages/recipesDetails'
import foods from '@/components/pages/foods'
import foodsResult from '@/components/pages/foodsResult'
import NProgress from 'nprogress'
Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: main
    },
    {
      path: '/recipes',
      name: 'recipes',
      component: recipes
    },
    {
      path: '*',
      name: '404',
      component: errorPage

    },
    {
      path: '/details',
      name: 'Recipes Details',
      component: recipesDetails
    },
    {
      path: '/foods',
      name: 'Foods Details',
      component: foods
    },
    {
      path: '/foodsResult',
      name: 'Foods Details',
      component: foodsResult
    }
  ],
  mode: 'history'
})

router.beforeResolve((to, from, next) => {
  if (to.name) {
    NProgress.start()
  }
  next()
})

router.afterEach((to, from) => {
  NProgress.done()
})

export default router