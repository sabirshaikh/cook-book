import axios from 'axios'
import NProgress from 'nprogress'
const recipesApi = axios.create({
  baseURL: 'https://api.edamam.com/',
  timeout: 10000
});

const foodsApi = axios.create({
  baseURL: 'https://api.edamam.com/api/food-database/',
  timeout: 10000,
});

recipesApi.interceptors.request.use(config => {
  NProgress.start()
  return config
})
// before a response is returned stop nprogress
recipesApi.interceptors.response.use(response => {
  NProgress.done()
  return response
})
foodsApi.interceptors.request.use(config => {
  NProgress.start()
  return config
})
// before a response is returned stop nprogress
foodsApi.interceptors.response.use(response => {
  NProgress.done()
  return response
})

export  {
  recipesApi,
  foodsApi
}