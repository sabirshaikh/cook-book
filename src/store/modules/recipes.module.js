import {recipesApi} from '../../apis/index';
import NProgress from 'nprogress'
const state = {
    recipes: [],
    foodItems: []
}
const mutations = {
    getRecipes (state, data) {
        state.recipes = data
        state.foodItems = data.data.hits
      },
    showMore (state, value) {
        var i;
        for(i=0; i < value.data.hits.length; i++ ) {
            state.foodItems.push(value.data.hits[i])
        }
    }
}
const actions = {
    getRecipes ({commit}, data) {
        return new Promise((resolve, reject) => {
          recipesApi.get('/search?q='+ data + '&app_id=87dc6b39&app_key=21b0439f73d40762540d12bb2dcccc9d&from=0&to=100')
            .then(res => {
              if(res.status == 200) {
                commit('getRecipes', res)
              }
              resolve(true);
            })
            .catch(error => {
              NProgress.done()
               reject(false);
            }) 
        })
    },
    getRecipesInfo ({commit}, data) {
        const config = {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        }
        return new Promise((resolve, reject) => {
          recipesApi.get('/search?r=' + data + '&app_id=87dc6b39&app_key=21b0439f73d40762540d12bb2dcccc9d', config)
            .then(res => {
              if(res.status == 200) {
                // commit('getRecipes', res)
              }
              resolve(res.data[0]);
            })
            .catch(error => {
              NProgress.done()
              reject(res)
            }) 
        })
    },
    showMore ({commit}, data) {
        return new Promise((resolve, reject) => {
          recipesApi.get('/search?q=' + data.q + '&app_id=87dc6b39&app_key=21b0439f73d40762540d12bb2dcccc9d&to='+ data.to +'&from='+ data.from)
            .then(res => {
              if(res.status == 200) {
                commit('showMore', res)
              }
              resolve(res);
            })
            .catch(error => {
              NProgress.done()
              reject(error)
            }) 
        })
    }
}
const getters = {
    getRecipes (state) {
        return state.recipes
      },
    getFoodItems (state) {
        return state.foodItems
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}