import {foodsApi} from '../../apis/index';
import NProgress from 'nprogress'
const state = {
    foods: []
}
const mutations = {
    getFoods (state, data) {
        state.foods = data
    }
}
const actions = {
    getFoods ({commit}, data) {
        return new Promise((resolve, reject) => {
          foodsApi.get('/parser?ingr='+ data + '&app_id=d18c01d3&app_key=131ee9573b7da583e64d47faf7a7670b')
            .then(res => {
              if(res.status == 200) {
                commit('getFoods', res)
              }
              resolve(true);
            })
            .catch(error => {
                NProgress.done()
              reject(false)
            }) 
        })
    }
}

const getters = {
    getFoods (state) {
        return state.foods
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}