import Vue from 'vue'
import Vuex from 'vuex'
import recipes from './modules/recipes.module'
import foods from './modules/food.module'
import createPersistedState from 'vuex-persistedstate'
Vue.use(Vuex)
export default new Vuex.Store({
  plugins: [createPersistedState()],
  modules: {
    recipesStore: recipes,
    foodsStore: foods
  }

})